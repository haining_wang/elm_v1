from flask import render_template
from apps.cms import cms_bp


@cms_bp.route('/', endpoint='index')
def cms_index():
    return render_template('index.html')

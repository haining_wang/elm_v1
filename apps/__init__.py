from flask import Flask


def register_cms_bp(app: Flask):
    from apps.cms import cms_bp
    app.register_blueprint(cms_bp)


def create_cms_app(config_info: str):
    app = Flask(__name__, static_folder='cms_statics')

    app.config.from_object(config_info)

    register_cms_bp(app)
    return app

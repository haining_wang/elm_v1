from apps import create_cms_app

cms_app = create_cms_app('apps.settings.CMSDevConfig')


if __name__ == '__main__':
    cms_app.run()
